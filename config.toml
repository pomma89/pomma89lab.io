baseURL = "https://alessioparma.xyz"
title = "Alessio Parma"
theme = "hugo-coder"
languageCode = "en"
defaultContentLanguage = "en"
paginate = 20
enableEmoji = true
# Enable Disqus comments
# disqusShortname = "yourdiscussshortname"

[markup.highlight]
style = "github-dark"

[params]
author = "Alessio Parma"
# license = '<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA-4.0</a>'
description = "Alessio Parma's personal website"
keywords = "developer,personal,pommalabs,projects,resume"
info = ["Back-end Developer", "Solution Architect"]
avatarURL = "/images/profile.256.jpg"
#gravatar = "john.doe@example.com"
dateFormat = "2 January 2006"
since = 2013
# Git Commit in Footer, uncomment the line below to enable it
commit = "https://gitlab.com/pomma89/pomma89.gitlab.io/tree/"
# Right To Left, shift content direction for languagues such as Arabic
rtl = false
# Specify light/dark colorscheme
# Supported values:
# "auto" (use preference set by browser)
# "dark" (dark background, light foreground)
# "light" (light background, dark foreground) (default)
colorScheme = "auto"
# Hide the toggle button, along with the associated vertical divider
hideColorSchemeToggle = false
# Series see also post count
maxSeeAlsoItems = 5
# Custom CSS
customCSS = []
# Custom SCSS, file path is relative to Hugo's asset folder (default: {your project root}/assets)
customSCSS = []
# Custom JS
customJS = []
# Custom remote JS files
customRemoteJS = []

# If you want to use fathom(https://usefathom.com) for analytics, add this section
# [params.fathomAnalytics]
# siteID = "ABCDE"
# serverURL = "analytics.example.com" # Default value is cdn.usefathom.com, overwrite this if you are self-hosting

# If you want to use plausible(https://plausible.io) for analytics, add this section
# [params.plausibleAnalytics]
# domain = "example.com"
# serverURL = "analytics.example.com" # Default value is plausible.io, overwrite this if you are self-hosting or using a custom domain
# outboundLinksTracking = true
# fileDownloadsTracking = true

# If you want to use goatcounter(https://goatcounter.com) for analytics, add this section
# [params.goatCounter]
# code = "code"

# If you want to use Cloudflare Web Analytics(https://cloudflare.com) for analytics, add this section
# [params.cloudflare]
# token = "token"

# If you want to use Baidu Analytics(https://tongji.baidu.com) for analytics, add this section
# [params.baidu]
# token = "token"

# If you want to use Matomo(https://matomo.org) for analytics, add this section
# [params.matomo]
# siteID = "ABCDE" # Default value is "1", overwrite this if you are cloud-hosting
# serverURL = "analytics.example.com" # For cloud-hosting, use provided URL, e.g. example.matomo.cloud

# If you want to use Google Tag Manager(https://analytics.google.com/) for analytics, add this section
# [params.googleTagManager]
# id = "gid"

# If you want to use Yandex Metrika(https://metrika.yandex.ru) for analytics, add this section
# [params.yandexMetrika]
# id = "gid"

# If you want to use Application Insights(https://azure.com/) for analytics, add this section
# [params.applicationInsights]
# connectionString = "connectionString"

# If you want to use microanalytics.io for analytics, add this section
# [params.microAnalytics]
# id = "ABCDE"
# dnt = "false" # respect DNT tracker, "true" by default

# If you want to use Pirsch(https://pirsch.io) for analytics, add this section
# [params.pirsch]
# code = "ABCDE"

# If you want to implement a Content-Security-Policy, add this section
[params.csp]
childsrc = ["'self'"]
fontsrc = ["'self'", "https://fonts.gstatic.com", "https://cdn.jsdelivr.net"]
formaction = ["'self'", "https://www.paypal.com"]
framesrc = ["'self'"]
imgsrc = ["'self'", "https://www.paypalobjects.com"]
objectsrc = ["'none'"]
scriptsrc = [
  "'self'",
  "'unsafe-inline'",
  "https://static.cloudflareinsights.com",
]
stylesrc = [
  "'self'",
  "'unsafe-inline'",
  "https://fonts.googleapis.com",
  "https://cdn.jsdelivr.net",
]
prefetchsrc = ["'self'"]
# connect-src directive – defines valid targets for to XMLHttpRequest (AJAX), WebSockets or EventSource
connectsrc = ["'self'"]

[taxonomies]
category = "categories"
series = "series"
tag = "tags"
author = "authors"

[[params.social]]
name = "Mastodon"
icon = "fa fa-2x fa-mastodon"
weight = 1
url = "https://hachyderm.io/@pomma89"
rel = "me"

[[params.social]]
name = "GitLab"
icon = "fa fa-2x fa-gitlab"
weight = 2
url = "https://gitlab.com/pommalabs"

[[params.social]]
name = "Docker Hub"
icon = "fa fa-2x fa-cubes"
weight = 3
url = "https://hub.docker.com/u/pommalabs"

[[params.social]]
name = "NuGet"
icon = "fa fa-2x fa-cogs"
weight = 4
url = "https://www.nuget.org/profiles/doktorp"

[[params.social]]
name = "Email"
icon = "fa fa-2x fa-envelope"
weight = 5
url = "mailto:alessio.parma@gmail.com"

[languages.en]
languageName = "English"

[[languages.en.menu.main]]
name = "Resume"
weight = 1
url = "resume/"

[[languages.en.menu.main]]
name = "Projects"
weight = 2
url = "projects/"

[[languages.en.menu.main]]
name = "PommaLabs"
weight = 3
url = "pommalabs/"

[languages.it-it]
languagename = "Italiano"

[languages.it-it.params]
info = ["Sviluppatore .NET", "Progettista Software"]
description = "Sito personale di Alessio Parma"
keywords = "sviluppatore,personale,pommalabs,progetti,curriculum,vitae"

[[languages.it-it.menu.main]]
name = "Curriculum Vitae"
weight = 1
url = "resume/"

[[languages.it-it.menu.main]]
name = "Progetti"
weight = 2
url = "projects/"

[[languages.it-it.menu.main]]
name = "PommaLabs"
weight = 3
url = "pommalabs/"

[markup.goldmark.renderer]
# Allow inserting raw HTML in Markdown files.
unsafe = true
