+++
title = "Projects"
slug = "projects"
+++

I maintain a few personal open source projects, 
whose sources are published on [my GitLab profile](https://gitlab.com/pomma89)
and on [PommaLabs GitLab group](https://gitlab.com/pommalabs).

## Warranty

Everything done on my projects is freely offered on the terms of each project license. 
You are free to do everything you want with the source code and its related files, 
as long as you respect the license and use common sense while doing it.

I maintain my open source projects during my spare time, which is getting smaller and smaller as time passes.
In other words, I can offer limited assistance but I cannot absolutely offer __any kind of warranty__.

## Donatations

If my projects have helped you, then you might offer me an hot cup of coffee (or two, if you feel generous):

{{< buttons/donate >}}
