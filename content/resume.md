+++
title = "Resume"
slug = "resume"
+++

I am currently working for [EdiSoftware S.r.l.](https://www.edisoftware.it/),
an Italian software house which builds ERP products.

Please refer to my [resume](/documents/resume-italian.pdf) (Italian) to find more details about myself; 
if you are curious to know what I did for my thesis, below you can find everything you need:

* Thesis "Windows Azure Platform - Storage Side": 
  [essay](/documents/azure-thesis-essay.pdf) and [slides](/documents/azure-thesis-essay.pdf) (Italian).
* Master Thesis "Dessert: a discrete event simulator for the .NET platform": 
  [essay](/documents/dessert-thesis-essay.pdf) and [slides](/documents/dessert-thesis-essay.pdf) (Italian),
  [paper](/documents/dessert-thesis-paper.pdf) (English).
