+++
title = "PommaLabs"
slug = "pommalabs"
+++

PommaLabs is a fictional company under whose name I publish most of my open source projects.

![PommaLabs Logo](/images/pommalabs-logo.256.png)

Source code of PommaLabs projects is hosted on [GitLab](https://gitlab.com/pommalabs)
and I have also published some of them on [NuGet](https://www.nuget.org/packages?q=pommalabs).
Some projects also have Docker images, which have been published on [Docker Hub](https://hub.docker.com/u/pommalabs).

Logo has been generated using [Shopify Hatchful](https://hatchful.shopify.com/).

## Warranty

Please check [my Projects page]({{< ref "projects.md#warranty" >}}) for more information.

## Donatations

If PommaLabs projects have helped you, then you might offer me an hot cup of coffee (or two, if you feel generous):

{{< buttons/donate >}}