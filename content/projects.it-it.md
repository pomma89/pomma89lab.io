+++
title = "Progetti"
slug = "projects"
+++

Curo lo sviluppo di alcuni progetti open source personali, 
i cui sorgenti sono pubblicati sul [mio profilo GitLab](https://gitlab.com/pomma89)
e sul [gruppo PommaLabs su GitLab](https://gitlab.com/pommalabs).

## Garanzia

Tutto ciò che viene fatto sui miei progetti è offerto gratuitamente nei termini della licenza di ciascun progetto. 
Sei libero di fare tutto ciò che vuoi con il codice sorgente e i relativi file, 
purché tu rispetti la licenza e usi il buon senso mentre lo fai.

Mantengo i miei progetti open source durante il mio tempo libero, che diventa sempre più stretto col passare del tempo. 
In altre parole, posso offrire un'assistenza limitata ma non posso assolutamente offrire __alcun tipo di garanzia__.

## Donazioni

Se i miei progetti ti hanno aiutato, allora potresti offrirmi una tazzina di caffè (o due, se ti senti generoso):

{{< buttons/donate >}}