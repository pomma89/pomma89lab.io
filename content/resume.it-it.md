+++
title = "Curriculum Vitae"
slug = "resume"
+++

Attualmente lavoro per [EdiSoftware S.r.l.](https://www.edisoftware.it/),
una società italiana che si occupa dello sviluppo di soluzioni ERP.

Fai riferimento al mio [curriculum vitae](/documents/resume-italian.pdf) (italiano) per scoprire di più su di me;
se sei curioso di sapere che cosa ho fatto per le tesi universitarie, di seguito troverai ciò che ti serve:

* Tesi triennale "Windows Azure Platform - Storage Side": 
  [relazione](/documents/azure-thesis-essay.pdf) e [lucidi](/documents/azure-thesis-essay.pdf) (italiano).
* Tesi magistrale "Dessert: a discrete event simulator for the .NET platform": 
  [relazione](/documents/dessert-thesis-essay.pdf) e [lucidi](/documents/dessert-thesis-essay.pdf) (italiano),
  [articolo](/documents/dessert-thesis-paper.pdf) (inglese).