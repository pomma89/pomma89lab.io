+++
title = "PommaLabs"
slug = "pommalabs"
+++

PommaLabs è una società fittizia, sotto il cui nome pubblico la maggior parte dei miei progetti open source.

![PommaLabs Logo](/images/pommalabs-logo.256.png)

I codici sorgenti dei progetti PommaLabs sono pubblicati su [GitLab](https://gitlab.com/pommalabs)
e alcuni di quei progetti sono anche pubblicati su [NuGet](https://www.nuget.org/packages?q=pommalabs).
Alcuni progetti dispongono di immagini Docker, pubblicate su [Docker Hub](https://hub.docker.com/u/pommalabs).

Il logo è stato generato tramite [Hatchful di Shopify](https://hatchful.shopify.com/).

## Garanzia

Vedi la pagina dei [miei Progetti]({{< ref "projects.md#warranty" >}}) per maggiori informazioni.

## Donazioni

Se i progetti di PommaLabs ti hanno aiutato, allora potresti offrirmi una tazzina di caffè (o due, se ti senti generoso):

{{< buttons/donate >}}